import javax.swing.JOptionPane;

class Fabrica {
    
    public static void main(String[] args) {
        
        Coche car1 = new Coche();
        car1.avanzar(200);

        Coche car2 = new Coche("5678-AG");
        car2.tunear("rojo");
        car2.avanzar(300);

        Coche car3 = new Coche(5,5);
        car3.avanzar(400);
        car3.matricular("9012-HH");

        Coche car4 = new Coche("VW","Caddy","blanco");
        car4.matricular("3456-XS");
        car4.tunear();
        car4.avanzar(500);
        
        Coche[] coches = new Coche[4];
        coches[0] = car1;
        coches[1] = car2;
        coches[2] = car3;
        coches[3] = car4;

        int i = 1;
        for (Coche car : coches) {
            caracteristicas(car,i);
            i++;
        } 
    }

    public static void caracteristicas(Coche c, int num) {
        String techo;
        if (c.techoSolar == true) {
            techo = "Tiene techo solar";
        } else techo = "No tiene techo solar";
        JOptionPane.showMessageDialog(null, "Las caracteristicas del coche " + num + "\nMatricula: " + c.getMatricula() + 
        "\nMarca: " + c.getMarca() + "\nModelo: " + c.getModelo() + "\nColor: " + c.getColor() + "\n" + techo + "\nKm: " + 
        c.getKm() + "\nNum Puertas: " + c.getNumPuertas() + "\nNum Plazas: " + c.getNumPlazas());
    }
   
}