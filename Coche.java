import javax.swing.JOptionPane;

class Coche {
    private String matricula;
    private String marca;
    private String modelo;
    private String color;
    boolean techoSolar;
    float km;
    int numPuertas;
    int numPlazas;

    Coche() {
        matricula = "1234-DF";
        marca = "Seat";
        modelo = "Toledo";
        color = "azul";
        techoSolar = false;
        km = 0;
        numPuertas = 3;
        numPlazas = 5;  
    }

    Coche(String matricula) {
        setMatricula(matricula);
        marca = "Fiat";    
        modelo = "Uno";
        color = "blanco";
        techoSolar = false;
        km = 0;
        numPuertas = 3;
        numPlazas = 2;
    }

    Coche(int numPuertas, int numPlazas) {
        if (numPuertas <= 5 && numPlazas <= 7) {
            setNumPlazas(numPlazas);
            setNumPuertas(numPuertas);
        }
        matricula = " ";
        marca = "BMW";
        modelo = "850";
        color = "gris";
        techoSolar = false;
        km = 0;
    }

    Coche(String marca, String modelo, String color) {
        setColor(color);
        setMarca(marca);
        setModelo(modelo);
        matricula = " ";
        techoSolar = false;
        km = 0;
        numPuertas = 5;
        numPlazas = 7;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setKm(float km) {
        this.km = km;
    }

    public float getKm() {
        return km;
    }

    public void setNumPuertas(int numPuertas) {
        this.numPuertas = numPuertas;
    }

    public int getNumPuertas() {
        return numPuertas;
    }

    public void setNumPlazas(int numPlazas) {
        this.numPlazas = numPlazas;
    }

    public int getNumPlazas() {
        return numPlazas;
    }

    public void setTechoSolar(boolean techoSolar) {
        this.techoSolar = techoSolar;
    }

    public boolean getTechoSolar() {
        return techoSolar;
    }

    void avanzar(float km) {
        setKm(km);
        float total = 0;
        total = total + km;
        JOptionPane.showMessageDialog(null,"EL coche a aumentado " + km + " km. \n Tiene un total de " + total + " km.");
    }

    void tunear() {
        km = 0;
        setTechoSolar(true);
        JOptionPane.showMessageDialog(null,"Se le ha instalado techo solar al coche.");
    }

    void tunear(String color) {
        tunear();
        setColor(color);
    }

    void matricular(String matricula) {
        setMatricula(matricula);
    }
}